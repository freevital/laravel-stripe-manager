<?php

namespace Freevital\Stripe\Test;

use Freevital\Stripe\Providers\LaravelServiceProvider;
use Illuminate\Database\Schema\Blueprint;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();

        $this->setUpDatabase($this->app);
    }

    /**
     * {@inheritdoc}
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'sqlite');
        $app['config']->set('database.connections.sqlite', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);

        (new \Dotenv\Dotenv(__DIR__, '../.env'))->load();

        $app['config']->set('stripe.secret', env('STRIPE_SECRET'));
    }

    /**
     * {@inheritdoc}
     */
    protected function getPackageProviders($app)
    {
        return [LaravelServiceProvider::class];
    }

    /**
     * Set up the database.
     */
    protected function setUpDatabase($app)
    {
        $this->createOwners($app);

        $this->runMigrations();
    }

    /**
     * Create owners tables.
     *
     * @param $app
     */
    protected function createOwners($app)
    {
        $app['db']->connection()->getSchemaBuilder()->create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
        });

        $app[User::class]->create(['email' => 'one@gm.com']);
        $app[User::class]->create(['email' => 'two@gm.com']);
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    protected function runMigrations()
    {
        include_once __DIR__ . '/../database/migrations/create_stripe_tables.php.stub';

        (new \CreateStripeTables())->up();
    }
}
