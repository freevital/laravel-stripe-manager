<?php

namespace Freevital\Stripe\Test;

class AccountTest extends TestCase
{
    private $stripeManager;

    private $account;

    public function setUp()
    {
        parent::setUp();

        $this->stripeManager = $this->app['Freevital\Stripe\StripeManager'];

        $user = User::first();
        $this->account = $this->stripeManager->account($user);
    }

    public function tearDown()
    {
        $this->account->delete();

        parent::tearDown();
    }

    public function test_creating_unverified_managed_account()
    {
        $account = $this->createUnverifiedAccount();
        $response = $account->__toArray(true);

        $this->assertArraySubset([
            'object'       => 'account',
            'legal_entity' => [
                'verification' => [
                    'status' => 'unverified'
                ]
            ]
        ], $response);
    }

    public function test_creating_verified_managed_account()
    {
        $account = $this->createVerifiedAccount();
        $response = $account->__toArray(true);

        $this->assertArraySubset([
            'object'       => 'account',
            'legal_entity' => [
                'verification' => [
                    'status' => 'verified'
                ]
            ]
        ], $response);
    }

    /**
     * Create unverified stripe account.
     *
     * @return mixed
     */
    protected function createUnverifiedAccount()
    {
        return $this->account->create(['managed' => true]);
    }

    /**
     * Create verified stripe account.
     *
     * @return mixed
     */
    protected function createVerifiedAccount()
    {
        $file_obj = $this->stripeManager->uploadFile(__DIR__ . '/stubs/success.png', [
            'purpose' => 'identity_document'
        ]);

        return $this->account->create([
            'managed'          => true,
            'legal_entity'     => [
                'type'               => 'individual',
                'first_name'         => 'John',
                'last_name'          => 'Smith',
                'dob'                => [
                    'day'   => '01',
                    'month' => '01',
                    'year'  => '1990'
                ],
                'address'            => [
                    'city'        => 'Chicago',
                    'line1'       => 'st. some',
                    'postal_code' => '60001',
                    'state'       => 'Illinois'
                ],
                'personal_id_number' => '000000000',
                'verification'       => [
                    'document' => $file_obj->id
                ]
            ],
            'tos_acceptance'   => [
                'date' => time(),
                'ip'   => $this->app['request']->ip()
            ],
            'external_account' => [
                'object'              => 'bank_account',
                'country'             => 'US',
                'currency'            => 'usd',
                'account_number'      => '000123456789',
                'routing_number'      => '110000000',
                'account_holder_name' => 'John Smith'
            ]
        ]);
    }
}
