<?php

namespace Freevital\Stripe\Test;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * {@inheritdoc}
     */
    protected $fillable = ['email'];

    /**
     * {@inheritdoc}
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     */
    protected $table = 'users';
}
