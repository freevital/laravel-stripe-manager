<?php

namespace Freevital\Stripe;

use Exception;
use Freevital\Stripe\Models\Stripe as StripeModel;
use Illuminate\Database\Eloquent\Model;

abstract class StripeAbstract
{
    /**
     * Owner model instance.
     *
     * @var Model
     */
    protected $owner;

    /**
     * Stripe entity instance.
     *
     * @var mixed
     */
    protected $entity;

    /**
     * Stripe model instance.
     *
     * @var \Freevital\Stripe\Models\Stripe
     */
    protected $stripeModel;

    /**
     * Stripe entity id.
     *
     * @var string
     */
    protected $stripeId;

    /**
     * EntityAbstract constructor.
     *
     * @param Model $owner
     */
    public function __construct(Model $owner)
    {
        $this->setOwner($owner);
        $this->makeEntity();
        $this->makeStripeModel();
    }

    /**
     * Stripe entity class name.
     *
     * @return mixed
     */
    abstract public function entity();

    /**
     * Instantiate stripe entity.
     *
     * @return void
     */
    protected function makeEntity()
    {
        $className = $this->entity();

        $this->setEntity(new $className);
    }

    /**
     * Get stripe entity.
     *
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set stripe entity.
     *
     * @param $entity
     *
     * @return void
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * Instantiate stripe model.
     *
     * @return void
     */
    protected function makeStripeModel()
    {
        $owner = $this->getOwner();

        $data = [
            'owner_id'   => $owner->getKey(),
            'owner_type' => get_class($owner),
            'type'       => get_class($this)
        ];

        $model = StripeModel::where($data)->first();

        if (!$model) {
            $model = new StripeModel();
            $model->forceFill($data);
        }

        $this->stripeModel = $model;
    }

    /**
     * Get stripe model.
     *
     * @return StripeModel
     */
    public function getStripeModel()
    {
        return $this->stripeModel;
    }

    /**
     * Get owner model instance.
     *
     * @return Model
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set owner model instance.
     *
     * @param Model $owner
     *
     * @return void
     */
    public function setOwner(Model $owner)
    {
        $this->owner = $owner;
    }

    /**
     * Save stripe model.
     *
     * @param array $data
     *
     * @return StripeModel
     */
    public function saveStripeModel(array $data)
    {
        $model = $this->getStripeModel();
        $model->stripe_id = $data['id'];
        $model->fill($data);
        $model->save();

        return $model;
    }

    /**
     * Determine if stripe entity has id.
     *
     * @return bool
     */
    public function isEmpty()
    {
        return !isset($this->id);
    }

    /**
     * Retrieve stripe entity instance by id or owner's stripe id.
     *
     * @param string|null       $id
     * @param array|string|null $opts
     *
     * @return $this
     * @throws Exception
     */
    public function retrieve($id = null, $opts = null)
    {
        if (!$id) {
            if (!$this->getStripeModel()->exists) {
                throw new Exception('Undefined stripe id.');
            }

            $id = $this->getStripeModel()->getAttribute('stripe_id');
        }

        $entity = $this->getEntity()->retrieve($id, $opts);

        $this->setEntity($entity);

        return $this;
    }

    /**
     * Create stripe entity.
     *
     * @param array|null        $params
     * @param array|string|null $opts
     *
     * @return $this
     */
    public function create($params = null, $opts = null)
    {
        $entity = $this->getEntity()->create($params, $opts);

        $this->setEntity($entity);

        $this->saveStripeModel($entity->__toArray());

        return $this;
    }

    /**
     * Update stripe entity.
     *
     * @param array             $params
     * @param array|string|null $opts
     *
     * @return $this
     */
    public function update($params = [], $opts = null)
    {
        $entity = $this->getEntity();

        foreach ($params as $attr => $value) {
            $entity->{$attr} = $value;
        }

        $entity->save($opts);

        $this->saveStripeModel($entity->__toArray());

        return $this;
    }

    /**
     * Delete stripe entity.
     *
     * @return mixed
     * @throws Exception
     */
    public function delete()
    {
        if (!$this->isEmpty()) {
            $response = $this->getEntity()->delete();

            $this->getStripeModel()->delete();

            return $response;
        }

        throw new Exception('Non existing entity.');
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->getEntity()->{$name});
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        return $this->getEntity()->{$name};
    }

    /**
     * @param $name
     * @param $value
     *
     * @return mixed
     */
    public function __set($name, $value)
    {
        return $this->getEntity()->{$name} = $value;
    }

    /**
     * Method call proxy.
     *
     * @param string $method
     * @param array  $params
     *
     * @return mixed
     * @throws Exception
     */
    public function __call($method, $params)
    {
        return call_user_func_array([$this->getEntity(), $method], $params);
    }
}
