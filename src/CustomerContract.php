<?php

namespace Freevital\Stripe;

use Exception;
use Stripe\Charge;

interface CustomerContract
{
    /**
     * Charge customer by token or customer id.
     *
     * @param null $params
     * @param null $options
     *
     * @return Charge
     * @throws Exception
     */
    public function charge($params = null, $options = null);

    /**
     * Get bank accounts.
     *
     * @param array $options
     *
     * @return mixed
     */
    public function bankAccounts($options = []);

    /**
     * Get bank account by id.
     *
     * @param $id
     *
     * @return mixed
     */
    public function bankAccount($id);

    /**
     * Create bank account by token or bank account data.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function createBankAccount($data = []);

    /**
     * Update bank account by id.
     *
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function updateBankAccount($id, $data = []);

    /**
     * Delete bank account by id.
     *
     * @param $id
     *
     * @return mixed
     */
    public function deleteBankAccount($id);


    /**
     * Get credit/debit cards.
     *
     * @param array $options
     *
     * @return mixed
     */
    public function cards($options = []);

    /**
     * Get credit/debit card by id.
     *
     * @param $id
     *
     * @return mixed
     */
    public function card($id);

    /**
     * Create card by token or card data.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function createCard($data = []);

    /**
     * Update card by id.
     *
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function updateCard($id, $data = []);

    /**
     * Delete card by id.
     *
     * @param $id
     *
     * @return mixed
     */
    public function deleteCard($id);
}