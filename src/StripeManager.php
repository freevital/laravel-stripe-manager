<?php

namespace Freevital\Stripe;

use Illuminate\Database\Eloquent\Model;
use Stripe\FileUpload;
use Stripe\Stripe as StripeApi;

class StripeManager implements StripeManagerContract
{
    /**
     * StripeManager constructor.
     */
    public function __construct($app)
    {
        $key = $app['config']->get('services.stripe.secret');

        if (!$key) {
            $key = $app['config']->get('stripe.secret');
        }

        StripeApi::setApiKey($key);
    }

    /**
     * Retrieve the account instance.
     *
     * @param Model $owner
     *
     * @return Account
     */
    public function account(Model $owner)
    {
        return new Account($owner);
    }

    /**
     * Retrieve the customer instance.
     *
     * @param Model $owner
     *
     * @return Customer
     */
    public function customer(Model $owner)
    {
        return new Customer($owner);
    }

    /**
     * Upload a file to stripe.
     *
     * @param string $path
     * @param array  $data
     *
     * @return mixed
     */
    public function uploadFile($path, $data)
    {
        $file = fopen($path, 'r');

        $data = array_merge($data, compact('file'));

        return FileUpload::create($data);
    }
}
