<?php

namespace Freevital\Stripe\Providers;

use Freevital\Stripe\StripeManager;
use Illuminate\Support\ServiceProvider;

class LaravelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $config_path = __DIR__ . '/../../config/stripe.php';

        // Publish config
        $this->publishes([
            $config_path => config_path('stripe.php')
        ], 'config');

        // Publish migrations
        if (!class_exists('CreateStripeTables')) {
            $timestamp = date('Y_m_d_His', time());

            $this->publishes([
                __DIR__ . '/../../database/migrations/create_stripe_tables.php.stub'
                => $this->app->databasePath()."/migrations/{$timestamp}_create_stripe_tables.php",
            ], 'migrations');
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $config_path = __DIR__ . '/../../config/stripe.php';

        // Merge config
        $this->mergeConfigFrom($config_path, 'stripe');

        $this->app->singleton(StripeManager::class, function ($app) {
            return new StripeManager($app);
        });
    }
}
