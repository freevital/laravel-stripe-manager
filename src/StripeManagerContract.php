<?php

namespace Freevital\Stripe;

use Illuminate\Database\Eloquent\Model;

interface StripeManagerContract
{
    /**
     * Retrieve the account instance.
     *
     * @param Model $owner
     *
     * @return Account
     */
    public function account(Model $owner);

    /**
     * Retrieve the customer instance.
     *
     * @param Model $model
     *
     * @return $this
     */
    public function customer(Model $model);

    /**
     * Upload a file to stripe.
     *
     * @param string $path
     * @param array  $data
     *
     * @return mixed
     */
    public function uploadFile($path, $data);
}