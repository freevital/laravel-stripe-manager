<?php

namespace Freevital\Stripe;

class Account extends StripeAbstract implements AccountContract
{
    /**
     * {@inheritdoc}
     */
    public function entity()
    {
        return 'Stripe\Account';
    }

    /**
     * Determine if account can obtain charges.
     *
     * @return bool
     */
    public function isChargeable()
    {
        return $this->charges_enabled;
    }

    /**
     * Determine if account can obtain payouts.
     *
     * @return bool
     */
    public function isPayable()
    {
        return $this->payouts_enabled;
    }

    /**
     * Get bank accounts.
     *
     * @param array $options
     *
     * @return mixed
     */
    public function bankAccounts($options = [])
    {
        $options = array_merge($options, ['object' => 'bank_account']);

        return $this->external_accounts->all($options);
    }

    /**
     * Get bank account by id.
     *
     * @param $id
     *
     * @return mixed
     */
    public function bankAccount($id)
    {
        return $this->external_accounts->retrieve($id);
    }

    /**
     * Create bank account by token or bank account data.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function createBankAccount($data = [])
    {
        if ($token = array_get($data, 'token')) {
            $data = ['external_account' => $token];
        }

        return $this->external_accounts->create($data);
    }

    /**
     * Update bank account by id.
     *
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function updateBankAccount($id, $data = [])
    {
        $bankAccount = $this->bankAccount($id);

        foreach ($data as $attr => $value) {
            $bankAccount->{$attr} = $value;
        }

        return $bankAccount->save();
    }

    /**
     * Delete bank account by id.
     *
     * @param $id
     *
     * @return mixed
     */
    public function deleteBankAccount($id)
    {
        $bankAccount = $this->bankAccount($id);

        return $bankAccount->delete();
    }


    /**
     * Get credit/debit cards.
     *
     * @param array $options
     *
     * @return mixed
     */
    public function cards($options = [])
    {
        $options = array_merge($options, ['object' => 'card']);

        return $this->external_accounts->all($options);
    }

    /**
     * Get credit/debit card by id.
     *
     * @param $id
     *
     * @return mixed
     */
    public function card($id)
    {
        return $this->external_accounts->retrieve($id);
    }

    /**
     * Create card by token or card data.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function createCard($data = [])
    {
        if ($token = array_get($data, 'token')) {
            $data = ['external_account' => $token];
        }

        return $this->external_accounts->create($data);
    }

    /**
     * Update card by id.
     *
     * @param       $id
     * @param array $data
     *
     * @return mixed
     */
    public function updateCard($id, $data = [])
    {
        $card = $this->card($id);

        foreach ($data as $attr => $value) {
            $card->{$attr} = $value;
        }

        return $card->save();
    }

    /**
     * Delete card by id.
     *
     * @param $id
     *
     * @return mixed
     */
    public function deleteCard($id)
    {
        $card = $this->card($id);

        return $card->delete();
    }
}
